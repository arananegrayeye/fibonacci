package org.alvaro.fibonacci;

public class Fibonacci {

	public int compute(int numero) {
		int resultado = 0;
		if (numero < 3) {
			resultado = 1;
		}
		else if(numero<0){
			throw new RuntimeException("Error: received negative value (" +
			          numero +")") ;
		}
		else {
			   resultado = compute(numero - 2) + compute(numero - 1);
		}
		return resultado;

	}

}
