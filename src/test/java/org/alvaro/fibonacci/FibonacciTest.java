package org.alvaro.fibonacci;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;



public class FibonacciTest {
	Fibonacci fib;
	@Before
	public void executedBeforeEach(){
		fib=new Fibonacci();
	}
	@Test
	public void fib1shouldReturn1(){
		assertEquals(fib.compute(1),1);
	}
	@Test
	public void fib6shouldReturn8(){
		assertEquals(fib.compute(6), 8);
	}
	@Test (expected = RuntimeException.class)
	  public void shouldFibOfANegativeNumberRaiseAnException() {
	    fib.compute(-1) ;
	  }
	
    
}
